<?php


class MDB
{
    public static $data = '';
    public static $type = '';
    public static $getSql = 0;

    public static function __callStatic($name, $arguments)
    {
        // echo '调用不存在的--静态--方法名是：' . $name . '<br>参数是：';
        // print_r($arguments);

        self::$data = $arguments;
        self::$type = $name;
        return new self;
    }

    // public static function pdo_fetch($data,$type='pdo_fetch'){

    //     self::$data=$data;
    //     self::$type=$type;
    //     return new self; 
    // }
    public function getSql()
    {
        if (self::$type == 'pdo_fetch' || self::$type == 'pdo_fetchall' ||  self::$type == 'pdo_fetchcolumn') {
            $retsql = self::$data[0];
            foreach (self::$data[1] as $k => $d) {
                $retsql = str_replace($k, (is_numeric($d) ? $d : "'" . $d . "'"), $retsql);
            }
        }

        //insert 
        if (self::$type == 'pdo_insert') {
            $insert_keys = [];
            $insert_datas = [];
            foreach (self::$data[1] as $k => $d) {
                $insert_keys[] = $k;
                $insert_datas[] = '\'' . $d . '\'';
            }
            $insert_keys = implode(',', $insert_keys);
            $insert_datas = implode(',', $insert_datas);



            $condition = implode(' and ', $condition);
            $condition = $condition ? ' and ' . $condition : '';

            $retsql = "INSERT INTO  " . tablename(self::$data[0]) . " ( " . $insert_keys . " ) VALUES (" . $insert_datas . " ) ";
        }

        //update
        if (self::$type == 'pdo_update') {
            $update_data = [];
            foreach (self::$data[1] as $k => $d) {
                $update_data[] = $k . "='" . $d . "'";
            }
            $update_data = implode(',', $update_data);


            $condition = [];
            if (!empty(self::$data[2])) {
                foreach (self::$data[2] as $c_k => $c_d) {
                    $condition[] = $c_k .  "='" . $c_d . "'";
                }
            }

            $condition = implode(' and ', $condition);
            $condition = $condition ? ' and ' . $condition : '';

            $retsql = "UPDATE " . tablename(self::$data[0]) . " SET " . $update_data . " WHERE 1 " . $condition . " ";
        }

        if (self::$type == 'pdo_get' || self::$type == 'pdo_getall') {
            $condition = [];
            if (!empty(self::$data[1])) {
                foreach (self::$data[1] as $c_k => $c_d) {
                    $condition[] = $c_k . "='" . $c_d . "'";
                }
            }

            $condition = implode(' and ', $condition);
            $condition = $condition ? ' and ' . $condition : '';

            $retsql = "SELECT * FROM " . tablename(self::$data[0]) . "  WHERE 1 " . $condition;
            if (self::$type == 'pdo_get') {
                $retsql .=  "  LIMIT 1 ";
            }
        }


        echo $retsql;
        // die;
        // return $retsql;
    }
    public function get()
    {
        $model = self::$type;
        if (self::$getSql == 1) {
            $this->getSql();
            echo "<br>";
        } else {
            switch ($model) {
                case 'pdo_fetchcolumn':

                    return $model(self::$data[0], self::$data[1]);

                    break;

                default:
                    return $model(self::$data[0], self::$data[1] ? self::$data[1] : array(), self::$data[2] ? self::$data[2] : array(), self::$data[3] ? self::$data[3] : 'AND', self::$data[4] ? self::$data[4] : '', self::$data[5] ? self::$data[5] : '', self::$data[6] ? self::$data[6] : '');
            }
        }
    }
}
