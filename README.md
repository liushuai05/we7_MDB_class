# 微擎sql调试类

#### 介绍
作为5年的微擎资深二开人员，从微擎刚刚开始出来就已经在使用这个框架。坚持使用这么久的原因是虽然底层代码写的不是很好，但是生态还是不错的，第三方模块也很丰富。但是二开的时候还是会遇到各种各样的问题，比如如何调试sql是一件很头疼的问题，虽然微擎官方给出了pdo_debug()方法但是这个方法输出的是所有已执行过的sql，并且没有解析pdo数组，每次调试都要睁大眼睛找到自己要调试的sql语句复制出来，然后还要手动拼接到phpmyadmin中进行测试。这样一波下来着实浪费时间。忍了4-5年了，现在0202年了实在不能忍了。所以今年年初我就设计了一个针对微擎sql输出的一个类库。直接输出的sql能在phpmyadmin中执行（已对应解析了pdo数组到sql语句中）。

#### 原理说明
实现原理非常简单，就是将微擎底层的pdo类前面多加了一个中间件，中转了一下，如果是要输出sql的地方就将sql拼接出来输出，如果不需要输出则直接调用底层的pdo类操作数据库。

#### 版权说明
此项目为MIT开源项目，此版权仅针对当前项目中的文件，此项目仅作为微擎的第三方外围扩展插件提供，此项目版权不代表微擎版权，使用此项目前请先购买正版微擎。



#### 安装教程

1.  将本项目中的 mdb.mod.php  复制到微擎的 /framework/model/  文件夹下
2.  在需要执行sql的前面加载这个类 load()->model('mdb');  


#### 使用方法1

1.  新增数据
```php
    $name=1;
    MDB::pdo_insert('表名',  array('name' => $name))
     ->getSql() //注释这一行则直接执行sql
    ->get();
    //输出内容 insert  into  表名 （name）values (1)
```
2.  查询数据
```php

$list = MDB::pdo_fetchall("select * from " . tablename("表名") . " where 1 uniacid=:uniacid",array(':uniacid'=>$_W['uniacid']))
     ->getSql() //注释这一行则直接执行sql 
    ->get();
    //输出内容 select * from  表名 where 1 uniacid=1 
```
3.  修改数据
```php
    MDB::pdo_update('表名',  array('name' => "张三"),array('id'=>2))
     ->getSql() //注释这一行则直接执行sql
    ->get();
    //输出内容 UPDATE  表名 set name="张三" where id=2
```




#### 使用方法2

1.  新增数据
```php
    MDB::$getSql=1;
    MDB::pdo_insert('表名',  array('name' => 1))
    ->get();
    MDB::$getSql=0;
    //输出内容 insert  into  表名 （name）values (1)
```
2.  查询数据
```php
    MDB::$getSql=1;
    $list = MDB::pdo_fetchall("select * from " . tablename("表名") . " where 1 uniacid=:uniacid",array(':uniacid'=>$_W['uniacid']))
    ->get();
    MDB::$getSql=0;
    //输出内容 select * from  表名 where 1 uniacid=1 
```
3.  修改数据
```php
    MDB::$getSql=1;
    MDB::pdo_update('表名',  array('name' => "张三"),array('id'=>2))
    ->get();
    MDB::$getSql=0;
    //输出内容 UPDATE  表名 set name="张三" where id=2
```

#### 参与贡献

1.  无

#### 特技
支持的方法如下
pdo_fetchall、pdo_fetch、pdo_get、pdo_getall、pdo_qruey、pdo_update、pdo_getcolumn